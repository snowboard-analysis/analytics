import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow_utils import gitlab_defaults
from kube_secrets import (
    SNOWFLAKE_ACCOUNT,
    SNOWFLAKE_LOAD_DATABASE,
    SNOWFLAKE_LOAD_PASSWORD,
    SNOWFLAKE_LOAD_ROLE,
    SNOWFLAKE_LOAD_USER,
    SNOWFLAKE_LOAD_WAREHOUSE,
    TRACE_CLIENT_KEY,
    TRACE_CLIENT_SECRET,
    TRACE_OAUTH_TOKEN,
    TRACE_OAUTH_TOKEN_SECRET,
)

# Load the env vars into a dict and set Secrets
env = os.environ.copy()
pod_env_vars = {"CI_PROJECT_DIR": "/analytics"}

# Default arguments for the DAG
default_args = {
    "catchup": False,
    "depends_on_past": False,
    # "on_failure_callback": slack_failed_task,
    "owner": "airflow",
    "retries": 1,
    "retry_delay": timedelta(minutes=1),
    "sla": timedelta(hours=12),
    # "sla_miss_callback": slack_failed_task,
    "start_date": datetime(2019, 1, 1),
}

# Create the DAG
dag = DAG("trace", default_args=default_args, schedule_interval="0 0 * * *")

REPO = "https://gitlab.com/snowboard-analysis/analytics.git"

trace_cmd = f"""
    git clone -b {env['GIT_BRANCH']} --single-branch {REPO} --depth 1 &&
    export PYTHONPATH="$CI_PROJECT_DIR/orchestration/:$PYTHONPATH" &&
    cd analytics/ &&
    python extract/trace_load.py {{{{ end_date }}}}
"""
trace = KubernetesPodOperator(
    **gitlab_defaults,
    image="registry.gitlab.com/snowboard-analysis/data-image/data-image:latest",
    task_id="trace",
    name="trace",
    secrets=[
        SNOWFLAKE_ACCOUNT,
        SNOWFLAKE_LOAD_DATABASE,
        SNOWFLAKE_LOAD_ROLE,
        SNOWFLAKE_LOAD_USER,
        SNOWFLAKE_LOAD_WAREHOUSE,
        SNOWFLAKE_LOAD_PASSWORD,
        TRACE_CLIENT_KEY,
        TRACE_CLIENT_SECRET,
        TRACE_OAUTH_TOKEN,
        TRACE_OAUTH_TOKEN_SECRET,
    ],
    env_vars=pod_env_vars,
    cmds=["/bin/bash", "-c"],
    arguments=[trace_cmd],
    dag=dag,
)
