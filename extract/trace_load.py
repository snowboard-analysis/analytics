import logging
import sys
from datetime import date, datetime
from os import environ

import pandas as pd
import pytz
from common import START, dataframe_uploader
from dateutil.parser import parse
from fire import Fire
from gitlabdata.orchestration_utils import (
    snowflake_engine_factory,
    snowflake_stage_load_copy_remove,
)
from py_trace import Trace

CHUNK = 100

VISITS_TABLE_NAME = "trace_visits"
RUNS_TABLE_NAME = "trace_runs"
logging.basicConfig(stream=sys.stdout, level=20)


def get_creds():
    creds = {
        "client_key": environ["TRACE_CLIENT_KEY"],
        "client_secret": environ["TRACE_CLIENT_SECRET"],
        "access_token": {
            "oauth_token": environ["TRACE_OAUTH_TOKEN"],
            "oauth_token_secret": environ["TRACE_OAUTH_TOKEN_SECRET"],
        },
    }
    return creds


def get_runs(visits):
    return pd.concat(
        visits.apply(
            lambda row: pd.DataFrame.from_records(row["runs"]), axis="columns"
        ).tolist()
    )


def get_last_date(engine):
    try:
        df = pd.read_sql(
            "select max(date) as date from {}".format(VISITS_TABLE_NAME), engine
        )
        max_dt = df["date"].max()
        return max_dt if isinstance(max_dt, datetime) else START
    except Exception:
        return START


def get_visits(start_date, end_date):
    trace_client = Trace(**get_creds())
    visits = []
    day = 60 * 60 * 24

    while start_date < end_date:
        logging.info(
            f"Pulling data between {datetime.fromtimestamp(start_date + day)} and {datetime.fromtimestamp(end_date)}"
        )
        # we start pulling the day after the start
        new_visits = trace_client.get_visits(
            limit=CHUNK, min_timestamp=start_date + day, max_timestamp=end_date
        )
        visits += new_visits

        if len(new_visits) == 0:
            break

        max_date = max(visit["date"] for visit in new_visits)
        logging.info(f"Max date fetched: {max_date}")
        start_date = to_timestamp(max_date)
        logging.info(f"New start_date timestamp {start_date}")

    if visits:
        df = pd.DataFrame.from_records(visits)
        df["date"] = pd.to_datetime(df["date"])
        return df


def to_timestamp(dt):
    if isinstance(dt, str):
        dt = parse(dt)
    elif isinstance(dt, int):
        return dt

    if isinstance(dt, (datetime, date)):
        return dt.replace(tzinfo=pytz.timezone("US/Mountain")).timestamp()
    else:
        raise Exception(f"dt of type: {type(dt)}")


def main(end_date=None):
    end_date = to_timestamp(end_date if end_date else datetime.today())
    logging.info(f"Starting sync ending in {end_date}")
    config_dict = environ.copy()

    engine = snowflake_engine_factory(config_dict, "LOADER")

    start_date = to_timestamp(get_last_date(engine))

    visits = get_visits(start_date, end_date)

    if visits is not None:
        dataframe_uploader(visits, engine, VISITS_TABLE_NAME, "public")
        runs = get_runs(visits)

        if runs is not None:
            dataframe_uploader(runs, engine, RUNS_TABLE_NAME, "public")


if __name__ == "__main__":
    Fire(main)
