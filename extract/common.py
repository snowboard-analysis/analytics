import re
from datetime import datetime
from time import time

START = datetime(2019, 1, 1)


def dataframe_uploader(dataframe, engine, table_name, schema_name):
    """Upload a dataframe, adding in some metadata and cleaning up along the way."""

    dataframe["_uploaded_at"] = time()  # Add an uploaded_at column
    dataframe = dataframe.applymap(
        lambda x: x if not isinstance(x, (dict, list)) else str(x)
    )  # convert dict to str to avoid snowflake errors

    dataframe = dataframe.applymap(
        lambda x: x[:4_194_304] if isinstance(x, str) else x
    )  # shorten strings that are too long
    dataframe.to_sql(
        name=table_name,
        con=engine,
        index=False,
        schema=schema_name,
        if_exists="append",
        chunksize=10000,
    )


def camel_to_snake(name):
    s = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    s = re.sub("([a-z0-9])([A-Z])", r"\1_\2", s).lower()
    return re.sub("_+", "_", s.replace(" ", "_"))
