from base64 import b64encode
from datetime import datetime
from os import environ
from subprocess import run

import pandas as pd
from common import START, camel_to_snake, dataframe_uploader
from dateutil.parser import parse
from fire import Fire
from fitbit import Fitbit
from gitlabdata.orchestration_utils import (
    snowflake_engine_factory,
    snowflake_stage_load_copy_remove,
)

CHUNK = 100


def b64encodestr(string):
    return b64encode(str(string).encode("utf-8")).decode()


def update_secrets(secret, key, val):
    b64val = b64encodestr(val)
    cmd = f"""kubectl patch secret {secret} -p='{{"data":{{"{key}": "{b64val}"}}}}'"""
    return run(cmd, shell=True)


def update_token(token):
    update_secrets("airflow", "FITBIT_ACCESS_TOKEN", token["access_token"])
    update_secrets("airflow", "FITBIT_REFRESH_TOKEN", token["refresh_token"])
    update_secrets("airflow", "FITBIT_EXPIRES_AT", token["expires_at"])


def get_creds():
    return {
        "client_id": environ["FITBIT_CLIENT_ID"],
        "client_secret": environ["FITBIT_CLIENT_SECRET"],
        "access_token": environ["FITBIT_ACCESS_TOKEN"],
        "refresh_token": environ["FITBIT_REFRESH_TOKEN"],
        "expires_at": float(environ["FITBIT_EXPIRES_AT"]),
    }


def get_fitbit_activity_measure(client, measure, start, end):
    res = client.time_series("activities/" + measure, base_date=start, end_date=end)
    data = res["activities-" + measure]
    if len(data):
        df = pd.DataFrame.from_records(data)
        df["dateTime"] = pd.to_datetime(df["dateTime"])
        return df[df["dateTime"] > start]
    else:
        return []


def get_fitbit_sleep(client, start, end):
    # to make this more "general purpose", we could take the end date and
    # do pagination calculations...
    from math import ceil

    num_days = (end - start).days
    num_calls = ceil(num_days / CHUNK)
    url = (
        "https://api.fitbit.com/1.2/user/-/sleep/list.json?"
        "offset=0&limit={chunk}&sort=asc&"
        "afterDate={start_date}"
    ).format(start_date=start.date(), chunk=CHUNK)
    data = []
    fields = ["timeInBed", "minutesAsleep", "dateOfSleep"]

    for i in range(num_calls):
        res = client.make_request(url)
        url = res["pagination"]["next"]
        data.extend(
            [
                {k: v for k, v in record.items() if k in fields}
                for record in res["sleep"]
            ]
        )
    if len(data):
        df = pd.DataFrame.from_records(data)
        df["dateOfSleep"] = pd.to_datetime(df["dateOfSleep"])
        return df[df["dateOfSleep"] > start]
    else:
        return []


def get_last_date(engine, table, date_field="date"):
    try:
        df = pd.read_sql(
            'select max("{date_field}") as "{date_field}" from {table}'.format(
                date_field=date_field, table=table
            ),
            engine,
        )
        max_dt = df[date_field].max()
        return max_dt if isinstance(max_dt, datetime) else START
    except Exception:
        return START


def update_activities(engine, client, end):
    activity_fields = [
        "calories",
        "minutesFairlyActive",
        "minutesLightlyActive",
        "minutesVeryActive",
        "steps",
    ]
    for measure in activity_fields:
        table_name = "fitbit_" + camel_to_snake(measure)
        start = get_last_date(engine, table_name, "dateTime")
        df = get_fitbit_activity_measure(client, measure, start, end)
        if len(df):
            print("Loading {} size: {}".format(table_name, len(df)))
            dataframe_uploader(df, engine, table_name, "public")


def update_sleep(engine, client, end):
    table_name = "fitbit_sleep"
    start = get_last_date(engine, table_name, "dateOfSleep")

    df = get_fitbit_sleep(client, start, end)
    if len(df):
        print("Loading {} size: {}".format(table_name, len(df)))
        dataframe_uploader(df, engine, table_name, "public")


def main(end_date=None):
    end_date = parse(end_date) if end_date else datetime.today()
    config_dict = environ.copy()

    engine = snowflake_engine_factory(config_dict, "LOADER")

    client = Fitbit(**get_creds(), refresh_cb=update_token)

    update_activities(engine, client, end_date)
    update_sleep(engine, client, end_date)


if __name__ == "__main__":
    Fire(main)
