{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('trace', 'trace_visits') }}

), renamed AS (

  SELECT
        date as date_time,
        air_height * 3.281 as air_height_ft,
        air_time as air_time_s,
        avg_slope as avg_slope,
        avg_speed * 2.23694 as avg_speed_mph,
        jumps as jumps,
        lift_time * 60 as lift_time_mins,
        lunch_time * 60 as lunch_time_mins,
        max_air_distance * 3.281 as max_air_distance_ft,
        max_air_height * 3.281 as max_air_height_ft,
        max_air_time as max_air_time_s,
        max_speed * 2.23694 as max_speed_mph,
        num_runs as num_runs,
        resort_title as resort,
        rest_time * 60 as rest_time_mins,
        season as season,
        slope_time * 60 as slope_time_mins,
        sustained_speed * 2.23694 as sustained_speed_mph,
        total_distance / 1609.344 as total_distance_mi,
        total_time * 60 as total_time_mins,
        vertical_drop * 3.281 as vertical_drop_ft
 FROM source
)

SELECT *
FROM renamed
