{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('trace', 'trace_runs') }}

), renamed AS (

  SELECT
        lunch_time * 60 as lunch_time_mins,
        vertical_drop * 3.281 as vertical_drop_ft,
        max_slope as max_slope,
        rest_time * 60 as rest_time_mins,
        jumps as jumps,
        max_air_time as max_air_time_s,
        max_speed * 2.23694 as max_speed_mph,
        total_distance / 1609.344 as total_distance_mi,
        sustained_speed * 2.23694 as sustained_speed_mph,
        air_height * 3.281 as air_height_ft,
        run_id as run_id,
        start_time as start_time,
        avg_slope as avg_slope,
        lift_time * 60 as lift_time_mins,
        avg_speed * 2.23694 as avg_speed_mph,
        max_air_height * 3.281 as max_air_height_ft,
        air_time as air_time_s,
        slope_time * 60 as slope_time_min,
        max_air_distance * 3.281 as max_air_distance_ft
  FROM source
)

SELECT *
FROM renamed
