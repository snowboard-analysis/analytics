
WITH calories AS (

  SELECT *
  FROM {{ ref('fitbit_calories')}}

)

, minutes_fairly_active
 AS (

  SELECT *
  FROM {{ ref('fitbit_minutes_fairly_active')}}

)

,minutes_lightly_active
 AS (

  SELECT *
  FROM {{ ref('fitbit_minutes_lightly_active')}}

)

, minutes_very_active AS (

 SELECT *
 FROM {{ ref('fitbit_minutes_very_active')}}

)

, sleep AS (

 SELECT *
 FROM {{ ref('fitbit_sleep')}}

)
, steps AS (

 SELECT *
 FROM {{ ref('fitbit_steps')}}

), dates AS (
  SELECT date_time
  FROM calories
  UNION
  SELECT date_time
  FROM minutes_very_active
  UNION
  SELECT date_time
  FROM minutes_very_active
  UNION
  SELECT date_time
  FROM minutes_very_active
  UNION
  SELECT date_time
  FROM sleep
  UNION
  SELECT date_time
  from steps
)
, unique_dates AS (
  SELECT distinct date_time
  FROM dates
)
, joined AS (
  SELECT
    dates.date_time as date_time,
    coalesce(calories.calories, 0) as calories,
    coalesce(steps.steps, 0) as steps,
    coalesce(minutes_fairly_active.minutes_fairly_active, 0) as minutes_fairly_active,
    coalesce(minutes_lightly_active.minutes_lightly_active, 0) as minutes_lightly_active,
    coalesce(minutes_very_active.minutes_very_active, 0) as minutes_very_active,
    coalesce(sleep.minutes_asleep, 0) as minutes_asleep,
    coalesce(sleep.minutes_in_bed, 0) as minutes_in_bed
  FROM
    dates left join calories on dates.date_time = calories.date_time
    left join minutes_fairly_active on dates.date_time = minutes_fairly_active.date_time
    left join minutes_very_active on dates.date_time = minutes_very_active.date_time
    left join minutes_lightly_active on dates.date_time = minutes_lightly_active.date_time
    left join sleep on dates.date_time = sleep.date_time
    left join steps on dates.date_time = steps.date_time

)

SELECT *
FROM joined
