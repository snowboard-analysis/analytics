{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('fitbit', 'fitbit_minutes_very_active') }}

), renamed AS (

  SELECT
        "dateTime" as date_time,
        value as minutes_very_active
 FROM source
)

SELECT *
FROM renamed
