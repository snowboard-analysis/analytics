{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('fitbit', 'fitbit_sleep') }}

), renamed AS (

  SELECT
        "dateOfSleep" as date_time,
        sum("minutesAsleep") as minutes_asleep,
        sum("timeInBed") as minutes_in_bed
 FROM source
 group by 1
)

SELECT *
FROM renamed
