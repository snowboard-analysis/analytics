{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('fitbit', 'fitbit_steps') }}

), renamed AS (

  SELECT
        "dateTime" as date_time,
        value as steps
 FROM source
)

SELECT *
FROM renamed
