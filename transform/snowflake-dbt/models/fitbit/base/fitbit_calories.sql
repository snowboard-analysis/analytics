{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('fitbit', 'fitbit_calories') }}

), renamed AS (

  SELECT
        "dateTime" as date_time,
        value as calories
 FROM source
)

SELECT *
FROM renamed
