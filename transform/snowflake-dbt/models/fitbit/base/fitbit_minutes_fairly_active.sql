{{ config({
    "schema": "staging"
    })
}}

WITH source AS (

  SELECT
    *
  FROM {{ source('fitbit', 'fitbit_minutes_fairly_active') }}

), renamed AS (

  SELECT
        "dateTime" as date_time,
        value as minutes_fairly_active
 FROM source
)

SELECT *
FROM renamed
